"use strict";
function Car(model, year, miles) {

    this.model = model;
    this.year = year;
    this.miles = miles;

    this.whenProduced = function () {
        return this.model + " is produced in " + this.year;
    };

    this.toString = function () {
        return this.model + " has done " + this.miles + " miles";
    };
}
let civic = new Car("Honda Civic", 2009, 20000);
let mondeo = new Car("Ford Mondeo", 2010, 5000);
mondeo.whenProduced = function () {
    return this.model + " permission denied";
};
let corsa = new Car("Corsa", 1999, 100000);

console.log(civic.toString());
console.log(mondeo.toString());
console.log(corsa.toString());

console.log(civic.whenProduced());
console.log(mondeo.whenProduced());
console.log(corsa.whenProduced());

/**
 ******************************************************************************************
 ******************************************************************************************
 ******************************************************************************************
 */

function ExtraCar(model, year, miles) {
    this.model = model;
    this.year = year;
    this.miles = miles;
}

ExtraCar.prototype.whenProduced = function () {
    return this.model + " is produced in " + this.year;
};
ExtraCar.prototype.toString = function   () {
    return this.model + " has done " + this.miles + " miles";
};
let civic = new ExtraCar("Honda Civic", 2009, 20000);
let mondeo = new ExtraCar("Ford Mondeo", 2010, 5000);
mondeo.whenProduced = function () {
    return this.model + " permission denied";
};
let corsa = new ExtraCar("Corsa", 1999, 100000);

console.log(civic.toString());
console.log(mondeo.toString());
console.log(corsa.toString());

console.log(civic.whenProduced());
console.log(mondeo.whenProduced());
console.log(corsa.whenProduced());
/**
 * AMD - Asynchronous Module Definition
 * https://mx.uat-la.m2-so.orbalab.com/customer/account/login/
 */
define( "orbaModule",
    ["jquery"],

    function ($) {
        let myModule = {
            countLogo: function () {
                return $('.logo').length;
            },
            countInputs: function(){
                return $('input').length;
            }
        };

        return myModule;
    });

define("orbaModule2", ["orbaModule"],function(oM){
    console.log('logo ', oM.countLogo());
    console.log('inputs ', oM.countInputs());
});

require(["orbaModule"],function(oM){
    console.log('logo ', oM.countLogo());
    console.log('inputs ', oM.countInputs());
});
/**
 * 1. What for? "private" and public methods
 * 2. Similar to IIFE but returns object instead of function
 */


let basketModule = (function () {

    let _basket = [];

    function addToBasket(item) {
        let cartData = false;
        if ('name' in item && 'price' in item) {
            _basket.push(item);
            cartData = getBasketData();
        }
        return cartData;
    }

    function _getItemsCount() {
        return _basket.length;
    }

    function _getBasketSum() {
        let sum = 0;
        _basket.forEach(function (item) {
            sum += item.price;
        });
        return sum;
    }

    function getBasketData() {
        return {
            itemsCount: _getItemsCount(),
            sum: _getBasketSum()
        }
    }

    return {
        addToBasket: addToBasket,
        getBasketData: getBasketData
    };
})();
console.log(basketModule.getBasketData());
basketModule.addToBasket({name: "water", price: 111});
basketModule.addToBasket({name: "orange", price: 333});
console.log(basketModule.getBasketData());
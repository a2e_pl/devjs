let myCar = {

    name: "Ford Escort",

    drive: function () {
        console.log("Weeee. I'm driving!");
    },

    panic: function () {
        console.log("Wait. How do you stop this thing?");
    }

};

// Use Object.create to instantiate a new car
let yourCar = Object.create(myCar);

// Now we can see that one is a prototype of the other
console.log('your1 ', yourCar.name);
console.log('my1 ', myCar.name);

myCar.name = 'Opel Corsa';
console.log('your2 ', yourCar.name);
console.log('my2 ', myCar.name);

yourCar.name = 'Fiat panda';
console.log('your3 ', yourCar.name);
console.log('my3 ', myCar.name);

/**
 https://blog.liip.ch/archive/2014/10/09/why-i-dont-use-the-javascript-new-keyword.html
 */
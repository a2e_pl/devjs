/**
 * 1. What for? "private" and public methods
 * 2. Similar to IIFE but returns object instead of function
 */


let basketModule = (function () {

    let _basket = [];

    function _getItemsCount() {
        return _basket.length;
    }

    function _getBasketSum() {
        let sum = 0;
        _basket.forEach(function (item) {
            sum += item.price;
        });
        return sum;
    }


    return {
        addToBasket: function (item) {
            let cartData = false;
            if ('name' in item && 'price' in item) {
                _basket.push(item);
                cartData = this.getBasketData();
            }
            return cartData;
        },
        getBasketData: function () {
            return {
                itemsCount: _getItemsCount(),
                sum: _getBasketSum()
            }
        }
    };
})();
console.log(basketModule.getBasketData());
basketModule.addToBasket({name: "water", price: 111});
basketModule.addToBasket({name: "orange", price: 333});
console.log(basketModule.getBasketData());